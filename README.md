# A11y Testing Workshop

## Prerequisite

* [Understand the basics of accessibility](https://medium.com/sparebank1-digital/a11y-101-8f1ebd20af3f)
* Bring a laptop & earphones
  * Install a [screen reader/s](#screen-readers)
  * Bring mobile and/or tablet (optional)

## Tools

### Static Analysis

#### HTML

* [W3C Validator](https://validator.w3.org/)
* HTML Validator ([Chrome extension](https://chrome.google.com/webstore/detail/html-validator/gcdopfgkcicgcekafjbnigbbeekacdgp))

#### WCAG

* [axe](https://www.deque.com/axe/) ([Chrome extension](https://chrome.google.com/webstore/detail/axe/lhdoppojpmngadmnindnejefpokejbdd) / [Firefox addon](https://addons.mozilla.org/en-US/firefox/addon/axe-devtools/))
* [WebAIM WAVE](http://wave.webaim.org/) ([Chrome extension](https://chrome.google.com/webstore/detail/wave-evaluation-tool/jbbplnpkjmmeebjpijfedlgcdilocofh) / [Firefox addon](https://addons.mozilla.org/en-US/firefox/addon/wave-accessibility-tool/))
* [Cynthia Says](http://www.cynthiasays.com/)

### Screen Readers

#### VoiceOver (OS X / iOS)

* Enable: System Preferences > Accessibility > Voice Over (Cmd+F5)
* [Cheat sheet](https://dequeuniversity.com/screenreaders/voiceover-keyboard-shortcuts)

#### [NVDA](https://www.nvaccess.org/download/) (Windows)

* [Cheat sheet](https://dequeuniversity.com/screenreaders/nvda-keyboard-shortcuts)

#### Narrator (Windows & IE/Edge)

* Enable: Settings > Easy of Access > Narrator (Win+Ctrl+N)
* [Cheat sheet](https://support.microsoft.com/en-us/help/22806/windows-10-narrator-keyboard-commands-touch-gestures)

#### TalkBack (Android)

* Enable: Press both volume keys for 3 seconds
* [Cheat sheet](https://support.google.com/accessibility/android/answer/6151827)

#### [ChromeVox](https://chrome.google.com/webstore/detail/chromevox/kgejglhpjiefppelpmljglcjbhoiplfn) (Chrome)

* [Cheat sheet](http://www.chromevox.com/keyboard_shortcuts.html)

#### [JAWS](https://www.freedomscientific.com/Downloads/JAWS)

* [Cheat sheet](https://dequeuniversity.com/screenreaders/jaws-keyboard-shortcuts)

#### [Orca](https://help.gnome.org/users/orca/stable/) (Linux)

## Task

1. Open the site you want to test
2. Test with [HTML tools](#html)
3. Test with [WCAG tools](#wcag)
4. Test keyboard navigation
5. Test content
6. Test with [screen reader/s](#screen-readers)
7. Report errors

## Certificate

[Certificate](https://gitlab.com/42tte/a11y/blob/master/Certificate.pdf)
